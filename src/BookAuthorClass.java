public class BookAuthorClass {
    public static void main(String[] args) {
        Author author1 = new Author("haiongngoai@gmail.com", "Hải", 'M');
        Author author2 = new Author("haiongngoai@yahoo.com", "Hoàng Hải", 'M');

        System.out.println("author1 : " + author1.toString() + "\n" +
                            "author2 :" + author2.toString());


        Book book1 = new Book("Hải" , author1 , 1.0);
        Book book2 = new Book("hải" , author2 , 2.0 , 10000);       
        

        System.out.println("book1 : " + book1.toString() + "\n" +
                            "book2 :" + book2.toString());
    }
}
