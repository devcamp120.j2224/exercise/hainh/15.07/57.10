public class Author {
    String email ;
    String name ;
    char gender ;

    public String getName() {
        return name;
    }


    public char getGender() {
        return gender;
    }


    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        this.email = email;
    }


    public Author(String email, String name, char gender) {
        this.email = email;
        this.name = name;
        this.gender = gender;
    }


    @Override
    public String toString() {
        return "Author [email=" + email + ", gender=" + gender + ", name=" + name + "]";
    }
    
}
